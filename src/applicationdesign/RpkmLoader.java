/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationdesign;


import java.io.File;
import java.io.FileNotFoundException;


/**
 *
 * @author raozinga & prfolkersma
 */
public class RpkmLoader {

    public static void main(String[] args) {
        RpkmLoader rpkmLoader = new RpkmLoader();
    }
    /**
     * This function checks the type of the entered file. If its not a .txt 
     * file it will throw an error else it will return true.
     * @param rpkmFileName
     * rpkmFileName is a string that contains the file name.
     * @return 
     * returns a boolean.
     */
    public boolean checkRpkmFileType(String rpkmFileName) {
        //Gets the last 4 characters of rpkmFileName and saves it
        String fileType = rpkmFileName.substring(rpkmFileName.length() - 4);
        boolean isTxt = false;
        //If fileType equals .txt then isTxt is true.
        if (".txt".equals(fileType)) {
            isTxt = true;
        } else {
            //in case the file doesn't end with .txt an error will be thrown.
            throw new IllegalArgumentException("Filetype was not .txt. Please"
                    + " load a .txt file");
        }
        return isTxt;

    }
    
    /**
     * This 
     * @param rpkmFilePath
     * @return
     * @throws FileNotFoundException 
     */
    public boolean checkRpkmPath(String rpkmFilePath) throws
            FileNotFoundException {
        if (new File(rpkmFilePath).exists()) {
            boolean pathExists = true;
            return pathExists;
        } else {
            throw new FileNotFoundException("Entered path does not exist");
        }
    }

}
