/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationdesign;

import java.util.ArrayList;

/**
 *
 * @author raozinga & prfolkersma
 */
public class GeneSearcher {
    public static void main(String[] args) {
        
    }
    /**
     * Searches a given arraylist for the given string and returns the matches.
     * @param searchName 
     * a string containing the gene the user wants to search for.
     * @param dataArray
     * the array with the gene names
     * @return 
     * an arraylist containing the matches of the search.
     */
    public ArrayList seachGene(String searchName, ArrayList dataArray) {
        ArrayList searchResults = new ArrayList();
        // goesd through every object of the array.
        for (Object gene : dataArray) {
            // truns both the gene and the searchword to lowercase.
            if(gene.toString().toLowerCase().contains(searchName.toLowerCase())){
                 searchResults.add(gene);
            }
        }
        //if there are no results an NullPointerException.
        if (searchResults.isEmpty()) {
           throw new NullPointerException("No Results where found");
        } else {
           return searchResults;
        }            
    }
}