/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationdesign;

import java.util.HashMap;

/**
 *
 * @author raozinga & prfolkersma
 */
public class ShowPopUp {
    
    public static void main(String[] args) {
        ShowPopUp spu = new ShowPopUp();
    }
    /**
     * Selects the information that will be put in the Pop up.
     * @param data
     * A HashMap containing the gene names and the data about the genes.
     * @param name
     * the name of the gene that was clicked.
     * @return 
     * The data of the name that was clicked 
     */
    public String selectInformation(HashMap<String, String> data, String name) {
        String information = data.get(name);
        return information;
    }
}
