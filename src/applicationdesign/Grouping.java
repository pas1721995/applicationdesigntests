/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationdesign;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author raozinga & prfolkersma
 */
public class Grouping {
    public static void main(String[] args) {
        Grouping gr = new Grouping();
    }
    
    /**
     * Groups the data bases on a search pattern.
     * @param data
     * Genes and their expressions in an HashMap.
     * @param findgroup
     * the pattern that will be grouped on.
     * @return 
     */
    public List group(HashMap<String,List<Integer>> data,
            List<Integer> findgroup) {
        List results = new ArrayList();
        Iterator it = data.entrySet().iterator();
        // while the HashMap has data the data will be checked with the pattern.
        // if it matches it will be added to an arrayList.
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if (pair.getValue().equals(findgroup)) {
                results.add(pair.getKey());
            }
            it.remove();
        }
        return results;
    }
}
