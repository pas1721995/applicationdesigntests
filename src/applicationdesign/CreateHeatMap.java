/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationdesign;

import java.util.HashMap;

/**
 * Creates a heat map.
 * @author raozinga & prfolkersma
 */
public class CreateHeatMap {
    public static void main(String[] args) {
        CreateHeatMap map = new CreateHeatMap();
    }
    /**
     * This function checks if the data for the heatmap is not empty or 
     * if the key has values. If not then an exception will be thrown.
     * @param heatMapData 
     * contains a hashmap <string,arraylist> with the genename and the values
     * @return 
     * true will be returned if neither the if or else if are triggered.
     */
    public boolean isValidHeatMapData(HashMap heatMapData) {
        // if theres no data in the hashmap and exception will be thrown.
        if (heatMapData.isEmpty()) {
            throw new IllegalArgumentException("The hashmap is empty");
        // if the hashmap has only key's and the values are empty then this 
        // exception will be thrown
        } else if (heatMapData.values().iterator().next().toString()
                .equalsIgnoreCase("[]")) {
            throw new IllegalArgumentException("The values in the hashmap "
                    + "are empty");
            
        }
        //true is returned
        return true;
    }
}
