/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationdesign;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author raozinga & prfolkersma
 */
public class GroupingTest {
    
    public GroupingTest() {
    }
    

    /**
     * Test of group method, of class Grouping.
     */
    @Test
    public void testGroup() {
        System.out.println("check if values are returned in a list");
        HashMap<String, List<Integer>> data = new HashMap();
        data.put("test1", Arrays.asList(1, 4, 1));
        data.put("test2", Arrays.asList(1, 4, 1));
        data.put("test3", Arrays.asList(2, 4, 1));
        data.put("test4", Arrays.asList(4, 4, 1));
        data.put("test5", Arrays.asList(9, 4, 1));
        List<Integer> findgroup = Arrays.asList(1, 4, 1);
        Grouping instance = new Grouping();
        List expResult = Arrays.asList("test2","test1");
        List result = instance.group(data, findgroup);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testAnotherEntry() {
        System.out.println("check if there are no values");
        HashMap<String, List<Integer>> data = new HashMap();
        data.put("test1", Arrays.asList(1, 4, 1));
        data.put("test2", Arrays.asList(1, 4, 1));
        data.put("test3", Arrays.asList(2, 4, 1));
        data.put("test4", Arrays.asList(4, 4, 1));
        data.put("test5", Arrays.asList(9, 4, 1));
        List<Integer> findgroup = Arrays.asList(7,3,2);
        Grouping instance = new Grouping();
        List expResult = Arrays.asList();
        List result = instance.group(data, findgroup);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEmptyMap() {
        System.out.println("check if there is no return with empty map");
        HashMap<String, List<Integer>> data = new HashMap();
        List<Integer> findgroup = Arrays.asList(7,3,2);
        Grouping instance = new Grouping();
        List expResult = Arrays.asList();
        List result = instance.group(data, findgroup);
        assertEquals(expResult, result);
    }
    
    @Test(expected = NullPointerException.class)
    public void testNullMap() {
        System.out.println("Check if the null exception is caught right.");
        HashMap<String, List<Integer>> data = null;
        List<Integer> findgroup = Arrays.asList(7,3,2);
        Grouping instance = new Grouping();
        List expResult = Arrays.asList();
        List result = instance.group(data, findgroup);
    }
}
