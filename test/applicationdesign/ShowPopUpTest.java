/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationdesign;

import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * 
 * @author raozinga & prfolkersma
 */
public class ShowPopUpTest {
    
    public ShowPopUpTest() {
    }
  
    @Test
    public void testSelectInformation() {
        System.out.println("normal use of the select information");
        HashMap<String, String> data = new HashMap();
        data.put("acde124", "gene that codes for imaginary stuff 1");
        data.put("acde114", "gene that codes for imaginary stuff 2");
        data.put("acde154", "gene that codes for imaginary stuff 3");
        data.put("acde134", "gene that codes for imaginary stuff 4");
        String name = "acde114";
        ShowPopUp instance = new ShowPopUp();
        String expResult = "gene that codes for imaginary stuff 2";
        String result = instance.selectInformation(data, name);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testWrongName() {
        System.out.println("search for gene that is not in the data");
        HashMap<String, String> data = new HashMap();
        data.put("acde124", "gene that codes for imaginary stuff 1");
        data.put("acde114", "gene that codes for imaginary stuff 2");
        data.put("acde154", "gene that codes for imaginary stuff 3");
        data.put("acde134", "gene that codes for imaginary stuff 4");
        String name = "lolhoi";
        ShowPopUp instance = new ShowPopUp();
        String expResult = null;
        String result = instance.selectInformation(data, name);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testNodata() {
        System.out.println("search for a gene when there is a hashmap but no data");
        HashMap<String, String> data = new HashMap();
        String name = "lolhoi";
        ShowPopUp instance = new ShowPopUp();
        String expResult = null;
        String result = instance.selectInformation(data, name);
        assertEquals(expResult, result);
    }
    @Test(expected = NullPointerException.class)
    public void testNullData() {
        System.out.println("search for gene with a datalist that is null");
        HashMap<String, String> data = null;
        String name = "lolhoi";
        ShowPopUp instance = new ShowPopUp();
        String expResult = null;
        String result = instance.selectInformation(data, name);
    }
}
