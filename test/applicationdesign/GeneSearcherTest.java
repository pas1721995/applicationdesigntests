/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationdesign;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author raozinga & prfolkersma
 */
public class GeneSearcherTest {
    
    public GeneSearcherTest() {
    }
    
    @Test 
    public void TestSeachGene() {
        System.out.println("normal use scenario of searchGene.");
        GeneSearcher search = new GeneSearcher();
        ArrayList arrayList = new ArrayList<>();
        arrayList.add("Koe");
        arrayList.add("Koe1");
        arrayList.add("Koe2");
        arrayList.add("koe3");
        arrayList.add("Egel");
        arrayList.add("Egel1");
        String searchName = "Koe";
        
        ArrayList expResult = new ArrayList<>();
        expResult.add("Koe");
        expResult.add("Koe1");
        expResult.add("Koe2");
        expResult.add("koe3");
        
        ArrayList result = search.seachGene(searchName, arrayList);
        assertEquals(expResult,result);
                
       
    }
    
    @Test(expected=NullPointerException.class)
    public void TestSearchGeneException() {
        System.out.println("When nothing has been found with searchGene.");
        GeneSearcher search = new GeneSearcher();
        ArrayList arrayList = new ArrayList<>();
        arrayList.add("Koe");
        arrayList.add("Koe1");
        arrayList.add("Koe2");
        arrayList.add("koe3");
        arrayList.add("Egel");
        arrayList.add("Egel1");
        String searchName = "paard";
        ArrayList result = search.seachGene(searchName, arrayList);
        
    }
    
    

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
