/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationdesign;

import java.io.FileNotFoundException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author raozinga & prfolkersma
 */
public class RpkmLoaderTest {

    public RpkmLoaderTest() {
    }

    @Test
    public void testCheckRpkmFileType() {
        System.out.println("normal use scenario of checkRpkmFileType.");
        RpkmLoader rpkmLoader = new RpkmLoader();
        String rpkmFileName = "rpkmData.txt";
        boolean expResult = true;
        boolean result = rpkmLoader.checkRpkmFileType(rpkmFileName);
        assertEquals(expResult, result);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckRpkmFileTypeException() {
        System.out.println("Exception where the file type isn't .txt");
        RpkmLoader rpkmLoader = new RpkmLoader();
        String rpkmInvalidFileName = "rpkmData.exe";
        boolean result = rpkmLoader.checkRpkmFileType(rpkmInvalidFileName);
    }

    @Test
    public void testCheckRpkmFilePath() throws FileNotFoundException {
        System.out.println("normal use scenario of checkRpkmFilePath");
        RpkmLoader rpkmLoader = new RpkmLoader();
        //a path that exists.
        String filePath = "/Users/Koeian/Desktop/test.txt";
        boolean expResult = true;
        boolean result = rpkmLoader.checkRpkmPath(filePath);
        assertEquals(expResult, result);
    }

    @Test(expected = FileNotFoundException.class)
    public void testCheckRpkmFilePathException() throws FileNotFoundException {
        System.out.println("normal use scenario of checkRpkmFilePath");
        RpkmLoader rpkmLoader = new RpkmLoader();
        //a path that doesn't exist.
        String filePath = "/Users/Koeian/Desktop/Harry.txt";
        boolean result = rpkmLoader.checkRpkmPath(filePath);
    }
}
