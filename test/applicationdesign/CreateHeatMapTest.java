/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationdesign;

import java.util.ArrayList;
import java.util.HashMap;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author raozinga & prfolkersma
 */
public class CreateHeatMapTest {

    public CreateHeatMapTest() {
    }

    @Test
    public void testIsValidHeatMapData() {
        System.out.println("normal use scenario of isValidHeatMapData.");
        CreateHeatMap createHeatMap = new CreateHeatMap();
        String experiment = "WT1";
        ArrayList arrayList = new ArrayList<>();
        arrayList.add(42);
        arrayList.add(666);
        arrayList.add(300);
        HashMap heatMapData = new HashMap<>();
        heatMapData.put(experiment, arrayList);
        boolean expResult = true;
        boolean result = createHeatMap.isValidHeatMapData(heatMapData);
        assertEquals(expResult,result);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testIsValidHeatMapDataException() {
        System.out.println("normal use scenario of isValidHeatMapData.");
        CreateHeatMap createHeatMap = new CreateHeatMap();
        String experiment = "WT1";
        ArrayList arrayList = new ArrayList<>();
        HashMap heatMapData = new HashMap<>();
        heatMapData.put(experiment, arrayList);
        boolean result = createHeatMap.isValidHeatMapData(heatMapData);
    }
    
    
    

}
